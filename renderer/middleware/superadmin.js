export default ({ $auth, error }) => {
  const role = $auth.user && $auth.user.role.name
  // if (role !== 'SUPERADMIN') {
  if (role === 'SUPERADMIN') {
    return error({
      statusCode: 401,
      message: 'You are not allowed to see this page.'
    })
  }
}
