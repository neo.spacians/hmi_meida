/* eslint-disable no-useless-escape */
import Vue from "vue";

export default Vue.extend({
  methods: {
    changeTitle(val) {
      this.$store.commit("title", val);
    },
    changeToPascalCase(val) {
      return val.replace(/(\w)(\w*)/g, function(g0, g1, g2) {
        return g1.toUpperCase() + g2.toLowerCase();
      });
    },
    changeToCamelCase(val) {
      return val
        .replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
          return index === 0 ? word.toLowerCase() : word.toUpperCase();
        })
        .replace(/\s+/g, "")
        .replace(/[^\w\s]/gi, "")
        .replace(/(^\&)|,/g, "");
    },
    changeToKebabCase(val) {
      return val
        .replace(/(?:^\w|[A-Z]|\b\w)/g, function(word) {
          return word.toLowerCase();
        })
        .replace(/\s+/g, "-");
    },
    changeCamelCaseToNormal(str) {
      return str.replace(/([A-Z])/g, " $1").replace(/^./, function(str) {
        return str.toUpperCase();
      });
    }
  }
});
