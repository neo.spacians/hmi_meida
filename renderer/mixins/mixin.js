import Vue from "vue";
import MomentMixin from "./Moment";
import CommonMixin from "./Common";

Vue.mixin(MomentMixin);
Vue.mixin(CommonMixin);
