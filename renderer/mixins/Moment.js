/* eslint-disable eqeqeq */
import moment from "moment";

export default {
  methods: {
    computedStringToDate(date, format = "YYYY-MM-DD", isDatetime = false) {
      const separator = date.includes("-") ? "-" : "/";
      let splitDate;
      let formattedDate;
      let splitTime = "";

      if (isDatetime) {
        const idxSpace = date.indexOf(" ");
        splitDate = date.substr(0, idxSpace).split(separator);
        splitTime = date.substr(idxSpace);
      } else {
        splitDate = date.split(separator);
      }

      if (format == "dd" + separator + "mm" + separator + "yyyy")
        formattedDate = new Date(
          splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0] + splitTime
        );
      else if (format == "mm" + separator + "dd" + separator + "yyyy")
        formattedDate = new Date(
          splitDate[2] + "-" + splitDate[0] + "-" + splitDate[1] + splitTime
        );
      else
        formattedDate = new Date(
          splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2] + splitTime
        );

      return formattedDate;
    },
    computedStringToMoment(date, format = "dd/mm/yyyy", isDatetime = false) {
      if (date) {
        const dateInput = moment(
          this.computedStringToDate(date, format, isDatetime)
        );
        return dateInput;
      } else {
        return null;
      }
    },
    computedMomentToString(date, format = "YYYY-MM-DD") {
      if (date) {
        return moment(date).format(format);
      } else {
        return null;
      }
    }
  }
};
