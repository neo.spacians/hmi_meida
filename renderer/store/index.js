export const strict = false

export const state = () => ({
  streams: [
    { id: 'SAFETY', name: 'Safety' },
    { id: 'QUALITY', name: 'Quality' },
    { id: 'COST', name: 'Cost' },
    { id: 'DELIVERY', name: 'Delivery' },
    { id: 'MOTIVATION', name: 'Motivation' },
    { id: 'ENVIRONMENT', name: 'Environment' }
  ],
  lines: [],
  areas: [],
  steps: [],
  title: null,
  listField: [
    {
      text: 'Text Title',
      value: 'textTitle'
    },
    {
      text: 'Text Description',
      value: 'textDesc'
    },
    {
      text: 'Props',
      value: 'props'
    },
    {
      text: 'Text Label',
      value: 'textLabel'
    },
    {
      text: 'Text Italic',
      value: 'textItalic'
    },
    {
      text: 'divider',
      value: 'divider'
    },
    {
      text: 'Avatar',
      value: 'avatar'
    },
    {
      text: 'Input Text',
      value: 'text'
    },
    {
      text: 'Input Number',
      value: 'number'
    },
    {
      text: 'Long Text',
      value: 'longtext'
    },
    {
      text: 'Stopwatch',
      value: 'stopwatch'
    },
    {
      text: 'Checkbox',
      value: 'checkbox'
    },
    {
      text: 'Multiple Choice',
      value: 'multiplechoice'
    },
    {
      text: 'Dropdown',
      value: 'dropdown'
    },
    {
      text: 'Date Picker',
      value: 'datepicker'
    },
    {
      text: 'Time Picker',
      value: 'timepicker'
    },
    {
      text: 'Date Time Picker',
      value: 'datetimepicker'
    },
    {
      text: 'Range Date',
      value: 'rangedate'
    },
    {
      text: 'Range Time',
      value: 'rangetime'
    },
    {
      text: 'Range Date Time',
      value: 'rangedatetime'
    },
    {
      text: 'Camera',
      value: 'camera'
    },
    {
      text: 'File',
      value: 'file'
    },
    {
      text: 'Video',
      value: 'video'
    }
  ],
  listPartDesign: ['textLabel', 'divider', 'avatar', 'textItalic', 'textTitle', 'textDesc'],
  listMetadata: ['text', 'number', 'checkbox', 'multiplechoice', 'dropdown', 'props'],
  listSelection: ['checkbox', 'dropdown', 'multiplechoice'],
  listRandomize: ['checkbox', 'dropdown', 'multiplechoice'],
  listAlarm: ['stopwatch', 'number', 'checkbox', 'multiplechoice', 'dropdown'],
  listDueDate: ['datepicker', 'timepicker', 'datetimepicker'],
  listClassification: [
    'stopwatch',
    'number',
    'timepicker',
    'datetimepicker',
    'checkbox',
    'dropdown',
    'multiplechoice'
  ],
  listUnit: ['number'],
  listFile: ['file', 'camera', 'video'],
  listTreshold: ['number', 'timepicker', 'datetimepicker', 'stopwatch'],
  listSection: [
    'number',
    'checkbox',
    'dropdown',
    'multiplechoice',
    'timepicker',
    'datetimepicker',
    'stopwatch'
  ],
  listUnits: [
    {
      text: 'KILOGRAM',
      value: 'kg'
    },
    {
      text: 'GRAM',
      value: 'g'
    },
    {
      text: 'METER',
      value: 'm'
    },
    {
      text: 'CENTIMETER',
      value: 'cm'
    },
    {
      text: 'PCS',
      value: 'pcs'
    },
    {
      text: 'BOX',
      value: 'box'
    },
    {
      text: 'Mpa',
      value: 'mpa'
    }
  ],
  listFileSize: [
    {
      text: '1 MB',
      value: 1000
    },
    {
      text: '5 MB',
      value: 5000
    },
    {
      text: '10 MB',
      value: 10000
    },
    {
      text: '15 MB',
      value: 15000
    },
    {
      text: '20 MB',
      value: 20000
    },
    {
      text: '25 MB',
      value: 25000
    }
  ],
  listFileCount: [
    {
      text: '5',
      value: 5
    },
    {
      text: '10',
      value: 10
    },
    {
      text: '15',
      value: 15
    }
  ],
  listStatement: [
    {
      text: 'Greater than (>)',
      value: '>'
    },
    {
      text: 'Greater than or equal to (>=)',
      value: '>='
    },
    {
      text: 'Less than (<)',
      value: '<'
    },
    {
      text: 'Less than or equal to(<=)',
      value: '<='
    },
    {
      text: 'Range (~)',
      value: '~'
    }
  ]
})

export const mutations = {
  lines(state, data) {
    state.lines = data
  },
  areas(state, data) {
    state.areas = data
  },
  steps(state, data) {
    state.steps = data
  },
  title(state, data) {
    state.title = data
  }
}

export const actions = {
  login(context, payload) {
    return new Promise((resolve, reject) => {
      this.$auth
        .loginWith('local', { data: payload })
        .then(() => {
          this.$toast.success('Welcome to OEE PT. Abbot!')
          resolve()
        })
        .catch(({ response: { data } }) => {
          this.$toast.error(data.message)
          reject(data.message)
        })
    })
  },
  logout() {
    return new Promise((resolve, reject) => {
      window.localStorage.clear()
      resolve()
    })
  },
  async lines({ commit }) {
    const { data } = await this.$axios.get('api/v1/lines')
    if (data.success) {
      commit('lines', data.data)
    }
  },
  async areas({ commit }) {
    const { data } = await this.$axios.get('api/v1/areas')
    if (data.success) {
      commit('areas', data.data)
    }
  },
  async steps({ commit }) {
    const { data } = await this.$axios.get('api/v1/steps')
    if (data.success) {
      commit('steps', data.data)
    }
  },
  title({ commit }, val) {
    commit('title', val)
  }
}
