export const strict = false
export const state = () => ({
  count: 1,
  label: 'Form Name',
  description: 'Form description',
  type: 'form',
  variableSubmit: [],
  notRequiredValue: [],
  category: {
    id: null,
    data: null
  },
  tag: null,
  sop: {
    id: null,
    data: null
  },
  step: {
    id: '',
    data: ''
  },
  area: {
    id: '',
    data: ''
  },
  line: {
    id: '',
    data: ''
  },
  post_url: null,
  redirect: null,
  sections: []
})

export const mutations = {
  increment(state) {
    state.count++
  },

  clearForm(state) {
    state.form = {
      label: 'Form Name',
      description: 'Form description',
      type: 'form',
      variableSubmit: [],
      notRequiredValue: [],
      category: {
        id: null,
        data: null
      },
      tag: null,
      sop: {
        id: null,
        data: null
      },
      step: {
        id: '',
        data: ''
      },
      area: {
        id: '',
        data: ''
      },
      line: {
        id: '',
        data: ''
      },
      post_url: null,
      redirect: null,
      sections: []
    }
  },

  addSection(state, section) {
    state.form.sections.push(section)
  },

  setForm(state, value) {
    state.form = value
  },

  setSection(state, value) {
    state.form.sections = value
  },

  setField(state, { value, index }) {
    state.form.sections[index].fields = value
  },

  removeSection(state, index) {
    state.form.sections.splice(index, 1)
  },

  addField(state, { index, field }) {
    state.form.sections[index].fields.push(field)
  },

  removeField(state, { index1, index2 }) {
    state.form.sections[index1].fields.splice(index2, 1)
  },

  showSection(state) {
    // eslint-disable-next-line no-console
    console.log(JSON.stringify(state.form.sections))
  }
}
