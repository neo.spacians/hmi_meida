require('dotenv').config()

export default {
  ssr: false,
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + 'OEE Abbot',
    title: 'Manufacturing Analytics (OEE)' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/png', href: '/favicon.png' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['./assets/main.scss', 'vue-json-pretty/lib/styles.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    './plugins/material-icons.js',
    './plugins/material-icons-md.js',
    './plugins/v-datetime-picker.js',
    './plugins/croppa.js',
    './mixins/mixin.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/eslint-module", "@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL || 'http://localhost:8000/'
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/v1/auths/login',
            method: 'post',
            propertyName: 'data.token'
          },
          user: {
            url: '/api/v1/current-user',
            method: 'get',
            propertyName: 'data'
          }
        }
      }
    },
    redirect: {
      login: '/login',
      callback: '/login',
      home: '/home'
    },
    cookie: false,
    rewriteRedirects: false,
    resetOnError: true
  },
  toast: {
    position: 'bottom-center',
    duration: 3000
  },
  router: {
    middleware: ['auth']
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: '#04368A',
          accent: '#315BE1',
          secondary: '#B44D03',
          info: '#03C5AF',
          warning: '#E17B31',
          error: '#FF1102',
          success: '#51B738'
        }
      }
    },
    defaultAssets: false
  },
  build: {
    extend: config => {
      config.target = "electron-renderer";
    }
  }
}
